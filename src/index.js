import React        from 'react';
import { render }   from 'react-dom';

import { Provider } from 'react-redux';
import store        from './redux/store';

import Game         from './Game';
import              './css/style.css';

const battleship = <Provider store={store}><Game/></Provider>;

render(battleship, document.getElementById('main'));