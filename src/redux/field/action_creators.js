import * as types       from './types';

export function setFieldSize(wide = 10, longitude = 10)
{
    return { type: types.SET_FIELD_SIZE, wide, longitude };
}