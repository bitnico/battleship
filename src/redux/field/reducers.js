import * as types from './types';

function field(state = {}, action)
{
    let players;

    switch (action.type) 
    {
        case types.SET_FIELD_SIZE:

            return {
                ...state,
                field: {...state.field, wide: action.wide, longitude: action.longitude }
            };

        default:
            return state;
    }
}

export default field;