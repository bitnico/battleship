import * as types from './field/types';

const error = store => next => action => {
    try         { next(action); }
    catch(e)    { console.log(e+' '+action); }
}

export default error;