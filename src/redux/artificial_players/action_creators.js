import * as types from './types';

export function useArtificialPlayerOne(active)
{
    return { type: types.ARTIFICIAL_PLAYER_ONE, active };
}

export function useArtificialPlayerTwo(active)
{
    return { type: types.ARTIFICIAL_PLAYER_TWO, active };
}

export function setArtificialPlayerAI(player, ai)
{
    switch (player) 
    {
        case '1':
            return { type: types.PLAYER_ONE_AI, ai };

        case '2':
            return { type: types.PLAYER_TWO_AI, ai };

        default:
            break;
    }
}