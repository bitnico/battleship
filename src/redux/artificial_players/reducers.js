import * as types from './types';

function artificialPlayers(state = {}, action)
{
    switch (action.type) 
    {
        case types.ARTIFICIAL_PLAYER_ONE: 
            return {...state, artificialPlayerOne: action.active};
        
        case types.ARTIFICIAL_PLAYER_TWO:
            return {...state, artificialPlayerTwo: action.active};
        
        case types.PLAYER_ONE_AI:
            return {...state, playerOneAI: action.ai};

        case types.PLAYER_TWO_AI:
            return {...state, playerTwoAI: action.ai};
        
        default: 
            return state;
    }
}

export default artificialPlayers;