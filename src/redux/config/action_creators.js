import * as types from './types';

export function posibleBoat(boat)
{
    return { type: types.POSIBLE_BOAT, boat };
}