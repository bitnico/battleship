import * as types       from './types';
import * as typesGame   from '../game/types';

function config(state = {}, action)
{
    switch (action.type) 
    {
        case types.POSIBLE_BOAT:
            return { ...state, posibleBoat: action.boat };

        case typesGame.FINISHED_GAME:
            if (action.result.surrendered) 
                return { ...state,
                    showSurrender:      false,
                    showRestart:        true,
                    hideEndScreen:      false
                };
            
            else if (action.result.playerWon) 
                return { ...state,
                    showSurrender:      false,
                    showRestart:        true,
                    hideEndScreen:      false
                };
            
            else 
                return { ...state,
                    showSurrender:      false,
                    showRestart:        true,
                    hideEndScreen:      false
                }
        
        case typesGame.START_GAME:
            return { ...state, 
                showInputPlayer:        action.configStart.showInputPlayer,
                showStart:              action.configStart.showStart,
                showSurrender:          action.configStart.showSurrender
            };

        default:
            return state;
    }
}

export default config;