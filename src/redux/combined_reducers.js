import { combineReducers }  from 'redux';

import artificialPlayers    from './artificial_players/reducers';
import players              from './players/reducers';
import field                from './field/reducers';
import game                 from './game/reducers';
import config               from './config/reducers';
import messages             from './messages/reducers';

const combinedReducers = combineReducers({ artificialPlayers, players, field, game, config, messages });

export default combinedReducers;