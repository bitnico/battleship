import * as types       from './types';
import * as typesPlayer from '../players/types';

function game(state = {}, action)
{
    switch (action.type)
    {
        case types.STATUS_MESSAGE:
            return {...state,  status: action.text };

        case types.START_GAME:
            return { ...state, 
                start: action.configStart.start,
                status: action.configStart.message
            };

        case types.FINISHED_GAME:
            if (action.result.surrendered)
                return { ...state, status: 'You have surrendered! shame on you..'};

            else if (action.result.playerWon) 
                return { ...state, status: 'Congratulations!! You are the winner!'};

            else 
                return { ...state, status: 'You have lost! I dont recomend to play again...'};

        case typesPlayer.STATE_OF_LAST_SHOOT:
            return { ...state, turnHistory: [...state.turnHistory, action.lastShoot] };

        default:
            return state;
    }
}

export default game;