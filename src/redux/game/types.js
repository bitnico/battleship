export const START_GAME         = 'START_GAME';
export const SET_PLAYER_TURN    = 'SET_PLAYER_TURN';
export const FINISHED_GAME      = 'FINISHED_GAME';
export const STATUS_MESSAGE     = 'STATUS_MESSAGE';