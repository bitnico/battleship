import * as types from './types';

export function startGame(configStart)
{
    return { type: types.START_GAME, configStart };
}

export function finishedGames(result)
{
    return { type: types.FINISHED_GAME, result };   
}

export function setStatusMessage(text)
{
    return { type: types.STATUS_MESSAGE, text };
}
