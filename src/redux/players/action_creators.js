import * as types       from './types';
import * as typesGame   from '../game/types';

export function setPlayerName(player = '1', name = '')
{
    switch (player) 
    {
        case '1':
            return { type: types.SET_PLAYER_ONE_NAME, name };

        case '2':
            return { type: types.SET_PLAYER_TWO_NAME, name };

        default:
            break;
    }
}

export function setPlayerBoats(player = '1', boats = [])
{
    switch (player) 
    {
        case '1':
            return { type: types.SET_PLAYER_ONE_BOATS, boats };

        case '2':
            return { type: types.SET_PLAYER_TWO_BOATS, boats };

        default:
            break;
    }
}

export function actualTurn(turn = '')
{
    return { type: types.SET_ACTUAL_TURN, turn };   
}

export function boatInserted(effectiveBoat, player)
{
    return { type: types.BOAT_INSERTED, effectiveBoat, player };
}

export function stateOfLastShoot(lastShoot, resultGrid)
{
    if (lastShoot.playerLose)
    {
        const result = { surrendered: false, playerWon: (lastShoot.player === '1') ? false : true };
        return { type: typesGame.FINISHED_GAME, result, resultGrid };
    }
    else
    {
        return { type: types.STATE_OF_LAST_SHOOT, lastShoot, resultGrid };
    }
}