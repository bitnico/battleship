import * as types       from './types';
import * as typesGame   from '../game/types';

function players(state = {}, action)
{
    let players = { ...state };

    switch (action.type) 
    {
        case types.SET_PLAYER_ONE_NAME:
            players['1'].name = action.name;
            return players;

        case types.SET_PLAYER_TWO_NAME:
            players['2'].name = action.name;
            return players;

        case types.SET_ACTUAL_TURN:
            players.actualTurn = action.player;
            return players;

        case types.SET_PLAYER_ONE_BOATS:
            players['1'].boats = action.boats;
            return players;
        
        case types.SET_PLAYER_TWO_BOATS:
            players['2'].boats = action.boats;
            return players;
    
        case types.BOAT_INSERTED:
            players[action.player].setBoatOnField(action.effectiveBoat.id);
            return players;

        case types.STATE_OF_LAST_SHOOT:
            let { lastShoot, resultGrid }           = action;
            players.actualTurn                      = (players.actualTurn === '1') ? '2' : '1';
            
            if     (lastShoot.playerTurn === '1')   players.lastShootResultP2 = lastShoot.message;
            else if(lastShoot.playerTurn === '2')   players.lastShootResultP1 = lastShoot.message;

            players[lastShoot.playerTurn].tempLastGrid = [...players[lastShoot.playerTurn].tempLastGrid, resultGrid];
            
            return players;

        case typesGame.START_GAME:
            players['1'].name                       = action.configStart.playerName;
            players.actualTurn                      = action.configStart.playerTurn;
            
            return  players;

        case typesGame.FINISHED_GAME:
            return { ...state, actualTurn: '' };
        default:
            return state;
    }
}

export default players;