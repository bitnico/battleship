import * as types from './types';

export function setEndScreenMessage(text)
{
    return { type: types.END_SCREEN_MESSAGE, text };
}

export function setHitShipText(text)
{
    return { type: types.HIT_SHIP_TEXT, text };
}

export function setDestroyShipText(text)
{
    return { type: types.DESTROY_SHIP_TEXT, text };
}

export function setMissShootText(text)
{
    return { type: types.MISS_SHOOT_TEXT, text };
}

export function setKillEnemyText(text)
{
    return { type: types.KILL_ENEMY_TEXT, text };
}

export function setSelectBoatText(text)
{
    return { type: types.SELECT_BOAT_TEXT, text };
}

export function setInsertBoatText(text)
{
    return { type: types.INSERT_BOAT_TEXT, text };
}

export function setOneClickText(text)
{
    return { type: types.ONE_CLICK_TEXT, text };
}