import * as types       from './types';
import * as typesGame   from '../game/types';

function messages(state = {}, action)
{
    switch (action.type)
    {
        case types.END_SCREEN_MESSAGE:
            return {...state, endScreen: action.text };

        case types.HIT_SHIP_TEXT:
            return {...state, onHitShip: action.text };

        case types.DESTROY_SHIP_TEXT:
            return {...state, onDestroyShip: action.text };

        case types.MISS_SHOOT_TEXT:
            return {...state, onMissShoot: action.text };

        case types.KILL_ENEMY_TEXT:
            return {...state, onKillEnemy: action.text };
            
        case types.SELECT_BOAT_TEXT:
            return {...state, selectBoat: action.text };

        case types.INSERT_BOAT_TEXT:
            return {...state, insertBoat: action.text };
            
        case types.ONE_CLICK_TEXT:
            return {...state, oneClick: action.text };

        case typesGame.FINISHED_GAME:
            if (action.result.surrendered)      return { ...state, endScreen: 'Maybe next time! or not..' };
            else if (action.result.playerWon)   return { ...state, endScreen: "That's my boy!!" };
            else                                return { ...state, endScreen: 'You are the worst.. this CPU is not that smart..' };                    

        default:
            return state;
    }
}

export default messages;