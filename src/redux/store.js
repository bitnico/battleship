import { createStore, compose, applyMiddleware }    from 'redux';
import combinedReducers                             from './combined_reducers';
import middlewares                                  from './middleware';
import Player                                       from '../classes/Player';

const defaultState = {

    game:
    {
        start:                      false,
        turnHistory:                [],
        tempLastShoot:              [],
        status:                     'Welcome! Setup your ships to war!'       
    },
    players:                        
    {
        actualTurn:                 '', 
        lastShootResultP1:          '',
        lastShootResultP2:          '',
        '1':                        new Player(),
        '2':                        new Player('CPU') 
    },
    field:
    {
        sizeAndNumberOfBoats:       new Map([ [ 3, 3], [2, 1], [4, 1] ]),
        wide:                       10,
        longitude:                  10
    },
    artificialPlayers:
    {
        artificialPlayerOne:        false,
        artificialPlayerTwo:        true,
        playerOneAI:                null,
        playerTwoAI:                null
    },
    config:
    {
        posibleBoat:                null,
        showInputPlayer:            true,
        showStart:                  true,
        showSurrender:              false,
        showRestart:                false,
        hideEndScreen:              true,
    },
    messages:
    {
        onHitShip:                  'Hit!',
        onDestroyShip:              'Ship destroyed!!',
        onMissShoot:                'Missed!',
        onKillEnemy:                'The player lost!',
        endScreen:                  '',
        selectBoat:                 'Click on a ship to select it!',
        insertBoat:                 'Go to the battlefield to position your ship!',
        oneClick:                   'Right click: Vertical, left click: Horizontal!'
    }
};

// const mw    = applyMiddleware(middlewares, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
// const store = createStore(combinedReducers, defaultState, mw);

//const mw    = applyMiddleware(middlewares);
const store = createStore(combinedReducers, defaultState,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;