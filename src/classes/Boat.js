

/****************************** Class Boat ******************************

    Representa cada bote en el juego, con su vida (health) 
    y caracteristicas. Se puede utilizar tanto como para mantener el
    registro de los botes presentes en una partida como para enviar
    enviar información de este tipo.

    La propiedad "extras" se utiliza en la configuración, y puede ser usada
    para enviar cualquier tipo de dato adicional que sea necesario.

*************************************************************************/

class Boat
{
    constructor(id = 0, size = 0, player = '0', onField = false, vertical = true)
    {
        this.id             = id;
        this._health        = size;
        this.size           = size;
        this.onField        = onField;
        this._vertical      = vertical;
        this.player         = player;

        this.extras         = '';
    }

    static createBoats(forPlayer, sizeAndNumber)
    {
        let listOfBoats = [];

        sizeAndNumber.forEach( (number, size) => {

            for(var i = 1; i <= number; i++)
            {
               listOfBoats.push(new Boat(listOfBoats.length, size, forPlayer, false, true));
            }
        });

        return listOfBoats;
    }

    isDeadAfterHit()
    {
        this._health--;

        if (this._health>0)  return false;
        else                 return true;
    }

    isDead()
    {
        if (this._health === 0) return true;
        else                 return false;
    }

    get health() {
        return this._health;
    }

    set vertical(vertical) {
        this._vertical = vertical;
    }

    get vertical() {
        return this._vertical;
    }

    set horizontal(horizontal) {
        this._vertical = !horizontal;
    }

    get horizontal() {
        return !this._vertical;
    }
}

export default Boat;