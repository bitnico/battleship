class Player 
{
    constructor(name = '', boats = [])
    {
        this.boats          = boats;
        this.name           = name;
        this.fullBoats      = false;
        this.tempLastGrid   = [];
        this.gridToShow     = null;
    }

    setBoatOnField(id)
    {
        let index                       = this.boats.findIndex(b => b.id === id);
        this.boats[index].onField       = true;

        let boatsLeft                   = this.boats.filter(boat => !boat.onField).length;
        if (!boatsLeft) this.fullBoats  = true;
    }
}

export default Player;