import Boat from './Boat';

/*********************************** Class Artificial Player ***********************************
 
   Esta clase posee las funcionalidades y logicas necesarias para que la CPU pueda juegar una
   partida, tanto contra un ser humano como contra instancia de esta misma clase.
 
***********************************************************************************************/

class ArtificialPlayer
{
    constructor(player, sizeAndNumber, fieldWide, fieldLongitude, makesBoatsVisible = false)
    {
        this.player             = player;
        this.fieldWide          = fieldWide;
        this.fieldLongitude     = fieldLongitude;
        
        this.numberOfLastShoot  = -2;
        this.searchMemory       = [];
        this.targetMemory       = this.initMemory(fieldWide, fieldLongitude);

        this.boats              = Boat.createBoats(player, sizeAndNumber);
        this.boats              = this.spinTheBoats(this.boats);
        
        this.thereAreBoatsLeft  = true;
        this.makesBoatsVisible  = makesBoatsVisible;

    }

    shoot = (battleField, numberOfShootsInGame) => {

        if ((this.numberOfLastShoot !== numberOfShootsInGame) && (this.numberOfLastShoot !== (numberOfShootsInGame + 1)))
        {
            this.makeTheShoot = () => {

                let hittedUnits = this.searchMemory.find(unit => (unit.result.hitted && !unit.result.destroyed));
                let x = false; let y = false;

                if (hittedUnits !== undefined)
                {
                    [x, y] = this.adyacentsCheck(hittedUnits);
                }
                if (!x && !y)
                {
                    x = this.randomNumber(this.fieldWide);  
                    y = this.randomNumber(this.fieldLongitude);
                    
                    while(this.targetMemory[x][y])
                    {
                        x = this.randomNumber(this.fieldWide);  
                        y = this.randomNumber(this.fieldLongitude);
                    }
                }  
                this.rememberThisPoint(x, y, battleField[x][y].touchTheUnit(false));
            }
            this.numberOfLastShoot  = numberOfShootsInGame;
            const delay             = this.randomNumber(3000);

            setTimeout(this.makeTheShoot.bind(this), delay);    
        }
    }

    adyacentsCheck(hittedUnits)
    {
        let [up, down, left, right] = [{x: 0, y: 0}, {x: 0, y: 0}, {x: 0, y: 0}, {x: 0, y: 0}];
        
        [up.x, down.x]      = [hittedUnits.x, hittedUnits.x];
        [left.y, right.y]   = [hittedUnits.y, hittedUnits.y];
        up.y                = (hittedUnits.y - 1 >= 0) ?                    hittedUnits.y - 1 : -1;
        down.y              = (hittedUnits.y + 1 < this.fieldLongitude) ?   hittedUnits.y + 1 : -1;
        left.x              = (hittedUnits.x - 1 >= 0) ?                    hittedUnits.x - 1 : -1;
        right.x             = (hittedUnits.x + 1 < this.fieldWide) ?        hittedUnits.x + 1 : -1;

        console.log(hittedUnits);
        console.log('x: '+down.x+' y: '+down.y);

        if      ((up.x !== -1)      && (up.y !== -1)    && (this.targetMemory[up.x][up.y] === undefined))       return [up.x, up.y];
        else if ((down.x !== -1)    && (down.y !== -1)  && (this.targetMemory[down.x][down.y] === undefined))   return [down.x, down.y];
        else if ((left.x !== -1)    && (left.y !== -1)  && (this.targetMemory[right.x][right.y] === undefined)) return [right.x, right.y];
        else if ((right.x !== -1)   && (right.y !== -1) && (this.targetMemory[left.x][left.y] === undefined))   return [left.x, left.y];
        else 
        {
            // Lo marcamos como destruido para no volver a chequear a su alrededor en el proximo turno

            let destroyUnit                 = this.searchMemory.find(unit => ((unit.x === hittedUnits.x) && (unit.y === hittedUnits.y)))
            destroyUnit.result.destroyed    = true;

            return [false, false];
        }
    }

    rememberThisPoint = (x, y, resultOfShoot) => {
        
        if (resultOfShoot !== undefined)
        {
            this.searchMemory.push({x: x, y: y, result: resultOfShoot});
            this.targetMemory[x][y] = resultOfShoot;
        }
    }

    initMemory(fieldWide, fieldLongitude)
    {
        let matrix = new Array(fieldWide);
        for (let y=0; y<fieldLongitude; y++) matrix[y] = new Array(fieldLongitude);
        return matrix;
    }

    spinTheBoats(boats)
    {
        for(let i=0; i<boats.length; i++) boats[i].vertical = Math.random() >= 0.5;
        return boats
    }

    randomNumber(number)
    {
        return Math.floor(Math.random() * number);
    }

    positionBoats(drawInGrid)
    {
        if (this.thereAreBoatsLeft)
        {
            let indexOfBoatOut  = this.boats.findIndex(boat => !boat.onField);

            if (indexOfBoatOut === -1) this.thereAreBoatsLeft = false;
            else
            {
                let type        = this.makesBoatsVisible ? 'visible' : 'initial';
                let x           = this.randomNumber(this.fieldWide);  
                let y           = this.randomNumber(this.fieldLongitude); 
                let newPaint    = { hasBoat: true, type: type, boatId: this.boats[indexOfBoatOut].id };

                if (!drawInGrid(x, y, newPaint, this.boats[indexOfBoatOut]))    this.positionBoats(drawInGrid)
                else                                                            this.boats[indexOfBoatOut].onField = true;
            }
        } 
    }

    thisUnitFieldIsDead(x, y)
    {
        let unitField = this.searchMemory.find(unit => ((unit.x === x) && (unit.y === y)));
        
        // Será undefinded en cuando se golpea la ultima seccion de un barco, esta sección aun no fué agregada y no está en la memoria, 
        // pero será agregada como muerta cuando llegue a rememberThisPoint el valor retonrnado por la función shootMade presente en BattleField.

        if (unitField !== undefined)
        {
            unitField.result.hitted     = false;
            unitField.result.destroyed  = true;
        } 
    }
}

export default ArtificialPlayer;