import React from 'react';

class MainTitle extends React.Component 
{
    render()
    {
       return (
            <header className="title">
                {this.props.title}
                <div className="subtitle"></div>
            </header>
        );
    }
}

export default MainTitle;