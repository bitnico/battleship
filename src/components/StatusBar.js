import React from 'react';

class StatusBar extends React.Component
{
    state = {
        playerName: ''
    }
    // Comienza un juego con las opciones que se hayan seleccionado

    newGame = () => {
        this.props.startGame(this.state.playerName);
    }

    // El parámetro surrendered es un booleano que indica si el juego finalizó por rendición

    endGame = surrendered => {
        this.props.finishedGame(surrendered);
    }

    // Reinicia el juego, una vez que haya terminido por rendición o con un ganador

    restartGame = () => {
        this.props.restartGame();
    }

    updatePlayerName(e) {
        this.setState({ playerName: e.target.value})
    }

    // Solo se muestran los controles necesarios indicados desde el nivel superior

    render()
    {
        return (
            <div className='statusBar'>
                <div className='statusMessage'>
                    {this.props.message}
                </div>
                <div className='statusButtons'>
                    {this.props.showInputPlayer && (<input 
                        type="text" className='text' placeholder="Your name"
                        onChange={e => this.updatePlayerName(e)} />)}
                    {this.props.showStart && (<div className='button start'
                        onClick={this.newGame}>Start Game</div>)}
                    {this.props.showSurrender && (<div className='button surrender'
                        onClick={e => this.endGame(true)}>Surrender! :(</div>)}
                    {this.props.showRestart && (<div className='button start'
                        onClick={e => this.restartGame()}>Restart the war!</div>)}
                </div>
            </div>
        )
    }
}

export default StatusBar;