import React                from 'react';

import ConfigSelectBoats    from './ConfigSelectBoats';
import ConfigInsertionMode  from './ConfigInsertionMode';
import Boat                 from '../classes/Boat';

class Config extends React.Component 
{
    constructor(props)
    {
        super(props);

        // Si los botes ya fueron creados, se cargan esos, sino, se crean nuevos

        if (this.props.playerBoats[this.props.player].length === 0) 
            this.props.insertEmptyBoats(Boat.createBoats(this.props.player, this.props.sizeAndNumberOfBoats), this.props.player);

        this.state = {
            vertical:       true,
            extras:         '',
            posibleBoat:    null,
            message:        this.props.selectBoatText
        }
    }

    posibleBoat = boat => {

        // Tomo el bote que seleccionó el usuario y le agrego la dirección seleccionada

        this.setState({ posibleBoat: boat, message: this.props.insertBoatText });

        boat.vertical   = this.state.vertical;
        boat.extras     = this.state.extras;

        this.props.posibleBoat(boat);
    }

    changeInsertionMode = mode => {
        
        // Tomo la dirección seleccionada por el usuario y la uno al bote anteriormente elegido

        let direction = {}

        switch (mode) {
            case 'vertical':
                direction = { vertical: true, extras: '' };
                break;
            case 'horizontal':
                direction = { vertical: false, extras: '' };
                break;
            case 'both':
                direction = { extras: 'both', message: this.props.oneClickText };
                break;
            default:
                console.log('Select a valid insertion mode.');
        }
        
        this.setState(direction);

        if (this.state.posibleBoat)
        {
            let posibleBoat         = {...this.state.posibleBoat};
            posibleBoat.vertical    = direction.vertical;
            posibleBoat.extras      = direction.extras;

            this.setState({ posibleBoat });
            this.props.posibleBoat(posibleBoat);
        }
        
    }

    valuesForInsertionMode()
    {
        if (this.state.extras === 'both')   return {vertical: false, horizontal: false, both: true};
        else                                return {vertical: this.state.vertical, horizontal: !this.state.vertical, both: false};
    }

    render()
    {
        const className  = this.props.hideScreen ? 'hide-screen' : 'wrap-config';
        const activeMode = this.valuesForInsertionMode();

        return (
            <div className={className}>
                
                <ConfigSelectBoats 
                    player          = {this.props.player}
                    boats           = {this.props.playerBoats[this.props.player]} 
                    posibleBoat     = {this.posibleBoat.bind(this)} 
                    boatSelected    = {this.state.posibleBoat}
                />
                
                <ConfigInsertionMode 
                    changeMode      = {this.changeInsertionMode.bind(this)} 
                    activeMode      = {activeMode}
                />
                
                <div className='helpMessage'>{this.state.message}</div>
                
            </div>
        )
    }
}

export default Config;