import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';

import * as fieldAC             from '../redux/field/action_creators';
import * as playersAC           from '../redux/players/action_creators';
import BattleField              from './BattleField';

const mapStateToProps = state => {

    return {
        playerOneAI:            state.artificialPlayers.playerOneAI,
        playerTwoAI:            state.artificialPlayers.playerTwoAI,
        
        playerBoats:            { '1': state.players['1'].boats, '2': state.players['2'].boats },
        posibleBoat:            state.config.posibleBoat,

        wide:                   state.field.wide,
        longitude:              state.field.longitude,
        sizeAndNumberOfBoats:   state.field.sizeAndNumberOfBoats,
        
        playerTurn:             state.players.actualTurn,
        turnHistory:            state.game.turnHistory,

        temporalGrid:           { '1': state.players['1'].gridToShow, '2': state.players['2'].gridToShow },
        
        messageOnHitShip:       state.messages.onHitShip,
        messageOnDestroyShip:   state.messages.onDestroyShip,
        messageOnMissShoot:     state.messages.onMissShoot,
        messageOnKillEnemy:     state.messages.onKillEnemy
    }
}

const mapDispachToProps = dispach => {

    return bindActionCreators({ ...fieldAC, ...playersAC }, dispach);
}

const BattleFieldConnected = connect(mapStateToProps, mapDispachToProps)(BattleField);

export default BattleFieldConnected;