import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';
import Config                   from './Config';
import * as configAC            from '../redux/config/action_creators';;

const mapStateToProps = state => {

    return {

        playerBoats:            { '1': state.players['1'].boats, '2': state.players['2'].boats },
        sizeAndNumberOfBoats:   state.field.sizeAndNumberOfBoats,
        
        selectBoatText:         state.messages.selectBoat,
        insertBoatText:         state.messages.insertBoat,
        oneClickText:           state.messages.oneClick,
    }
}

const mapDispachToProps = dispach => {

    return bindActionCreators(configAC, dispach);
}

const ConfigConnected = connect(mapStateToProps, mapDispachToProps)(Config);

export default ConfigConnected;