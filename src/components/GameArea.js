import React                from 'react';
import propTypes            from 'prop-types';

import PlayerSide           from './PlayerSide';
import ConfigConnected      from './Config_Connected';
import History              from './History';
import ArtificialPlayer     from '../classes/ArtificialPlayer'

class GameArea extends React.Component 
{
    constructor(props)
    {
        super(props);
        
        // Si las dos IA están participando entonces se muestran los botes en pantalla de ambas

        const showYourBoatsIA   = (this.props.artificialPlayerOne && this.props.artificialPlayerTwo) ? true : false;

        let artificialPlayerOne, artificialPlayerTwo = false;

        if (this.props.artificialPlayerOne)
        {
            artificialPlayerOne = new ArtificialPlayer('1', this.props.sizeAndNumberOfBoats, this.props.fieldWide, this.props.fieldLongitude, showYourBoatsIA);
            this.props.setArtificialPlayerAI('1', artificialPlayerOne);
            this.props.setPlayerBoats('1', artificialPlayerOne.boats);
        }

        if (this.props.artificialPlayerTwo)
        {
            artificialPlayerTwo = new ArtificialPlayer('2', this.props.sizeAndNumberOfBoats, this.props.fieldWide, this.props.fieldLongitude, showYourBoatsIA);
            this.props.setArtificialPlayerAI('2', artificialPlayerTwo);
            this.props.setPlayerBoats('2', artificialPlayerTwo.boats);
        }
    }

    // insertEmptyBoats es invocado desde el Config ya que el GameArea es el encargado de cargar los botes vacios, 
    // sean estos provenientes del usuario o de la inteligencia artificial.

    insertEmptyBoats = (listOfBoats, player) => {

        if (player === '1') this.props.setPlayerBoats('1', listOfBoats);
        if (player === '2') this.props.setPlayerBoats('2', listOfBoats);
    }

    showThisTurn = numberOfTurn => {
        
        const gridToShowP1 = this.state.tempLastGridP1[numberOfTurn];
        const gridToShowP2 = this.state.tempLastGridP2[numberOfTurn];

        this.setState({ gridToShowP1: gridToShowP1, gridToShowP2: gridToShowP2 });
    }

    // Las dos IA deben ir a los dos tableros, ya que en uno deben atacar y en el otro cargar sus botes.

    render()
    {
        const className = this.props.hideScreen ? 'hide-screen' : 'game-area';

        return (
            <div className={className}>
                <PlayerSide 
                    player                  = '1' 
                    message                 = {this.props.lastShootResultP1}
                />
                <ConfigConnected 
                    player                  = '1'
                    insertEmptyBoats        = {this.insertEmptyBoats.bind(this)}
                    hideScreen              = {this.props.inBattle}
                />
                <PlayerSide 
                    player                  = '2'
                    message                 = {this.props.lastShootResultP2}
                    hideScreen              = {!this.props.inBattle}
                />
                <History 
                    resultsHistory          = {this.props.tempLastShoot}
                    showThisTurn            = {this.showThisTurn.bind(this)} 
                />
            </div>
        );
    }
}

// GameArea.propTypes = 
// {
//     fieldWide:              propTypes.number.isRequired,
//     fieldLongitude:         propTypes.number.isRequired,
//     sizeAndNumberOfBoats:   propTypes.instanceOf(Map),


//     turnHistory:            propTypes.array,
//     nextTurn:               propTypes.func.isRequired,

//     selectBoatText:         propTypes.string,
//     insertBoatText:         propTypes.string,
//     oneClickText:           propTypes.string,
//     messageOnHitShip:       propTypes.string,
//     messageOnDestroyShip:   propTypes.string,
//     messageOnMissShoo:      propTypes.string,
//     messageOnKillEnemy:     propTypes.string,

//     inBattle:               propTypes.bool.isRequired,
//     fullBoats:              propTypes.func.isRequired,
//     finishedGame:           propTypes.func.isRequired,

//     artificialPlayerOne:    propTypes.bool.isRequired,
//     artificialPlayerTwo:    propTypes.bool.isRequired,

//     temporalGrids:          propTypes.object
// }

export default GameArea;