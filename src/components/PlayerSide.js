import React                from 'react';
import PropTypes            from 'prop-types';

import ArtificialPlayer     from '../classes/ArtificialPlayer';
import Boat                 from '../classes/Boat';
import BattleFieldConnected from './BattleField_Connected';

class PlayerSide extends React.Component 
{
    render()
    {
        const className     = this.props.hideScreen ? 'hide-screen' : 'wrap-playerside';
        const classResult   = (this.props.player === '1') ? 'hitResult blue' : 'hitResult red';

        return (
            <div className={className}>
                <BattleFieldConnected player = {this.props.player} />
                <div className={classResult}> {this.props.message} </div>
            </div>
        );
    }
}

// PlayerSide.propTypes = 
// {
//     player:                 PropTypes.string.isRequired,
//     playerTurn:             PropTypes.string.isRequired,
//     playerOneAI:            PropTypes.instanceOf(ArtificialPlayer),
//     playerTwoAI:            PropTypes.instanceOf(ArtificialPlayer),

//     fieldWide:              PropTypes.number.isRequired,
//     fieldLongitude:         PropTypes.number.isRequired,
//     sizeAndNumberOfBoats:   PropTypes.instanceOf(Map),

//     posibleBoat:            PropTypes.instanceOf(Boat),
//     boatInserted:           PropTypes.func.isRequired,
//     playerBoats:            PropTypes.array,
//     resultOfLastshoot:       PropTypes.func.isRequired,

//     messageOnHitShip:       PropTypes.string,
//     messageOnDestroyShip:   PropTypes.string,
//     messageOnMissShoot:     PropTypes.string,
//     messageOnKillEnemy:     PropTypes.string,
    
//     hideScreen:             PropTypes.bool
// }

export default PlayerSide;