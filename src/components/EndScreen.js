import React from 'react';

class EndScreen extends React.Component 
{    
    render()
    {
        const className = this.props.hideScreen ? 'hide-screen' : 'endScreen';
        return (
            <div className={className}>
                <div className='end-screen-congratulations'></div>
                <div className='end-screen-text'>{this.props.message}</div>
                <div className='end-screen-image'></div>
            </div>
        );
    }
}

export default EndScreen;