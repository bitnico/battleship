import React, { Fragment }  from "react";

import StatusBar            from './StatusBar';
import MainTitle            from './MainTitle';
import GameAreaConnected    from './GameArea_Connected';
import EndScreen            from './EndScreen';

class Main extends React.Component
{
    /************************************************************************************************
      
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  

      * * * * * * * * * * * * * * * * *   Main: React Battleship   * * * * * * * * * * * * * * * * * 

      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    ************************************************************************************************/

    componentWillUpdate(nextProps)
    {
       if(nextProps.players['1'].fullBoats && nextProps.players['2'].fullBoats && (this.props.game.status === 'Welcome! Setup your ships to war!')) this.props.setStatusMessage('Now you can fight!!');
    }

    startGame = playerName => {

        if (playerName === '')                          this.props.setStatusMessage('Enter your name!');
        else if (!this.props.players['1'].fullBoats)    this.props.setStatusMessage('You must position your ships!!');
        else if (this.props.players['2'].fullBoats)
        {
            let playerOneTurn   = Math.random() >= 0.5;
            let message         = playerOneTurn ? 'Playing: ' + playerName : 'Playing: ' + this.props.players['2'].name;
            let playerTurn      = playerOneTurn ? '1' : '2';
            const configStart   = { start: true, playerName, message, playerTurn, showInputPlayer: false, showStart: false, showSurrender: true };

            this.props.startGame(configStart);
        }
        else { alert('There is something wrong with the CPU!') }
    }
    restartGame = () => {

        window.location.reload()
    }

    render()
    {
        /* Configuraciones:

        Si se desea ver CPU vs CPU, se debe setear en 'true' la constante 'artificialPlayerOne', luego en el
        juego solo hay que ingresar nombre y dar play. */

        const appTitle                      = 'React Battleship';

        return (
            <Fragment>
                <MainTitle
                    title                   = {appTitle}
                />
                <StatusBar
                    startGame               = {this.startGame.bind(this)} 
                    restartGame             = {this.restartGame.bind(this)}

                    message                 = {this.props.statusBar}
                    showInputPlayer         = {this.props.config.showInputPlayer}
                    showRestart             = {this.props.config.showRestart}
                    showStart               = {this.props.config.showStart}
                    showSurrender           = {this.props.config.showSurrender}
                />
                <GameAreaConnected
                    inBattle                = {this.props.game.start}
                    hideScreen              = {!this.props.config.hideEndScreen}
                />
                <EndScreen
                    hideScreen              = {this.props.config.hideEndScreen}
                    message                 = {this.props.messages.endScreen}
                />
                <div className="footer">Copyright 2018 - Arzion® S.R.L.</div>
            </Fragment>
        )
    }
}

export default Main;