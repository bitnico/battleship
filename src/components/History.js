import React    from 'react';
import Turn     from './Turn.js';

class History extends React.Component 
{
    showThisTurn = selectedTurn => { this.props.showThisTurn(selectedTurn) }

    render()
    {
        const className = this.props.hideScreen ? 'hide-screen' : 'history';
        let turns       = [];

        for (let i=0; i < this.props.resultsHistory.length; i++)
        {
            if ((i+1)< this.props.resultsHistory.length)
            {
                let oneMatch = [this.props.resultsHistory[i], this.props.resultsHistory[i+1]]
                turns.push(oneMatch)
            }
        }

        return (<div className={className}>
                    { turns.map((turn, index) => (
                        <Turn 
                            numberOfTurn={index} 
                            showThisTurn={this.showThisTurn} 
                            onTurnSelect={this.showThisTurn} 
                            data={turn} 
                        />)
                    )}
                </div>);
    }
}

export default History;