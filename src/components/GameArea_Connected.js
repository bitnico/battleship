import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';

import * as configAC                from '../redux/config/action_creators';
import * as playersAC               from '../redux/players/action_creators';
import * as messagesAC              from '../redux/messages/action_creators';
import * as artificialPlayersAC     from '../redux/artificial_players/action_creators';
import GameArea                     from './GameArea';

const mapStateToProps = state => {

    return {

        playerOneBoats:         state.players['1'].boats,
        playerTwoBoats:         state.players['2'].boats,

        artificialPlayerOne:    state.artificialPlayers.artificialPlayerOne,
        artificialPlayerTwo:    state.artificialPlayers.artificialPlayerTwo,
        playerOneAI:            state.artificialPlayers.playerOneAI,
        playerTwoAI:            state.artificialPlayers.playerTwoAI,

        sizeAndNumberOfBoats:   state.field.sizeAndNumberOfBoats,
        fieldWide:              state.field.wide,
        fieldLongitude:         state.field.longitude,

        tempLastShoot:          state.game.tempLastShoot,
        tempLastGridP1:         state.players['1'].tempLastGrid,
        tempLastGridP2:         state.players['2'].tempLastGrid,
        gridToShowP1:           state.players['1'].gridToShow,
        gridToShowP2:           state.players['2'].gridToShow,

        lastShootResultP1:      state.players.lastShootResultP1,
        lastShootResultP2:      state.players.lastShootResultP2,

        selectBoatText:         state.messages.selectBoat,
        insertBoatText:         state.messages.insertBoat,
        oneClickText:           state.messages.oneClick
    }
}

const mapDispachToProps = dispach => {

    return bindActionCreators({ ...configAC, ...playersAC, ...artificialPlayersAC, ...messagesAC }, dispach);
}

const GameAreaConnected = connect(mapStateToProps, mapDispachToProps)(GameArea);

export default GameAreaConnected;