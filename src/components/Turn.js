import React from 'react';

class Turn extends React.Component 
{
    state = { resultsHistory: [] }

    showThisTurn = e => {
        this.props.showThisTurn(this.props.numberOfTurn);
    }

    render()
    {
        let messageP1 = this.props.data[0].message;
        let messageP2 = this.props.data[1].message;
        return (
            <div className='turn'>
                <div onClick={this.showThisTurn}>{messageP2+' '+messageP1}</div>
            </div>
        );
    }
}

export default Turn;