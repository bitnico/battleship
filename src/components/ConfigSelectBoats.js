import React        from 'react';
import ConfigBoat   from './ConfigBoat';

class ConfigSelectBoats extends React.Component 
{
    constructor(props)
    {
        super(props);

        if (this.props.boatSelected)
        {
            this.state = { boats : props.boats, selectedBoatId : this.props.boatSelected.id };
            this.boatSelected(this.props.boatSelected);
        }
        else{
            this.state = { boats : props.boats, selectedBoatId : null };
        }
    }

    boatSelected = boat => {
        this.setState({ selectedBoatId : boat.id });
        this.props.posibleBoat(boat);
    }

    render(){
        return (
            <div className='wrap-listOfBoats'>
                {this.props.boats.map(boat => (
                    <ConfigBoat 
                        player={this.props.player}
                        boat={boat} key={'p'+boat.player+'b'+boat.id} 
                        boatInserted={this.props.boatInserted}
                        onSelectBoat={this.boatSelected.bind(this)} 
                        selectedId={this.state.selectedBoatId}
                    />))}
            </div>
        )
    }
}

export default ConfigSelectBoats;