import React from 'react';

class ConfigInsertionMode extends React.Component 
{
    selectedMode(e) 
    {
        this.props.changeMode(e.target.id);
    }

    render(){

        const classForVertical      = this.props.activeMode.vertical ? 'insertMode selectedMode' : 'insertMode';
        const classForHorizontal    = this.props.activeMode.horizontal ? 'insertMode selectedMode' : 'insertMode';
        const classForBoth          = this.props.activeMode.both ? 'insertMode selectedMode' : 'insertMode';

        return (
            <div className='wrap-insertModes'>
                    <div id='vertical' className={classForVertical} onClick={e => { this.selectedMode(e) }} >Vertical</div>
                    <div id='horizontal' className={classForHorizontal} onClick={e => { this.selectedMode(e) }} >Horizontal</div>
                    <div id='both' className={classForBoth} onClick={e => { this.selectedMode(e) }} >1 Click</div>
            </div>
        )
    }
}

export default ConfigInsertionMode;