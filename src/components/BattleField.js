import React            from 'react';
import PropTypes        from 'prop-types';

import UnitField        from './UnitField';
import ArtificialPlayer from '../classes/ArtificialPlayer';
import Boat             from '../classes/Boat';



class BattleField extends React.Component
{
    /************************************** Class Battle Field **************************************
    
        Rrepresenta el campo de batalla, la grilla donde transcurre el juego. Se encarga de 
        gestionar los eventos ocurridos durante el juego, recibiendo mensajes de las UnitField, que
        son las unidades por las que está compuesto, y enviandole a las mismas si es necesario
        renderizar algún cambio. Como también enviando mensajes al padre referidos al estado actual
        del juego, y si el mismo ha finalizado o no.
    
    ************************************************************************************************/

    constructor(props)
    {
        super(props);

        // Las matrices son inicializadas afuera del elemento state debido a que debe completarse
        // la inicialización de la primer matriz para que se realize la segunda, ya que esta dispone
        // de la anterior.

        this.unitsGridRef   = this.initGridRefs(props.wide, props.longitude);
        this.unitsGrid      = this.initGrid(props.wide, props.longitude);
        this.killTheBoat    = this.killTheBoat.bind(this);
        this.canInsertBoat  = this.canInsertBoat.bind(this);
        this.drawInGrid     = this.drawInGrid.bind(this);

        this.state = {
            unitsGridRefs:  this.unitsGridRef,
            unitsGrid:      this.unitsGrid,
            tempUnitsGrid:  [],
            actualGrid:     null
        } 
    }

    componentDidMount()
    {
        // Si es el player 2 y tenemos una IA cargada, empezamos a posicionar los botes, igual con el 1.

        if ((this.props.player === '2') && (this.props.playerTwoAI))
        {
            this.props.playerTwoAI.positionBoats(this.drawInGrid);
        }
        else if ((this.props.player === '1') && (this.props.playerOneAI))
        {
            this.props.playerOneAI.positionBoats(this.drawInGrid);
        }
    }

    componentDidUpdate()
    {
        // Desde el punto de vista de la máquina, si se encuentra en el Battlefield
        // del player 2, debe cargar sus botes, y no perder, si se encuentra en el 1, debe atacar.

        console.log( this.props.playerTurn)

        if (!this.gameOver())
        {
            if ((this.props.player === '2') && (this.props.playerTwoAI))
            {    
                this.props.playerTwoAI.positionBoats(this.drawInGrid);
            }
            else if ((this.props.player === '1') && (this.props.playerOneAI))
            {
                this.props.playerOneAI.positionBoats(this.drawInGrid);
            }

            if ((this.props.player === '1') && (this.props.playerTurn === '2') && (this.props.playerTwoAI))
            {
                this.props.playerTwoAI.shoot(this.unitsGridRef, this.props.turnHistory.length);
            }
            else if ((this.props.player === '2') && (this.props.playerTurn === '1') && (this.props.playerOneAI))
            {
                this.props.playerOneAI.shoot(this.unitsGridRef, this.props.turnHistory.length);
            }
        }
        
        if ((this.props.temporalGrid) && (this.props.temporalGrid.length > 0) && (this.actualGrid !== null))
        {
            const actualGrid = [...this.state.unitsGrid];
            
            // back de unitGridRefs?
            this.setState({ unitsGrid: this.props.temporalGrid, actualGrid });
        }
    }

    initGrid(width, height) 
    {
        let grid = [];
        for (let y = 0; y < width; y++)
        {
            grid.push([]);
            
            for (let x = 0; x < height; x++)
            {
                grid[y].push(
                    <UnitField 
                        x={x} y={y} key={'x'+x+'y'+y} type='initial' 
                        shootMade={this.shootMade.bind(this)} paintBoat={this.paintBoat.bind(this)}
                        show={true} player={this.props.player} ref={unit => this.unitsGridRef[x][y] = unit}
                    />)
            }
        }
        return grid;
    }

    initGridRefs(width, height) 
    {
        let gridRef = new Array(width);
        for (let y=0; y<height; y++) gridRef[y] = new Array(height);
        return gridRef;
    }

    killTheBoat = (boat, section, x, y) => {
        
        /* Se busca la sección inicial del barco restando a la posición en la que se efectuó 
        el disparo su número de sección, a partir de ahí se tildan como eliminadas las unidades
        siguientes hasta llegar al numero de unidades que posee el barco.*/

        if (boat.vertical)
        {
            let actualSection = y - section;
            const lastSection = actualSection + boat.size;

            for (actualSection; actualSection < lastSection; actualSection++)
            {
                this.state.unitsGridRefs[x][actualSection].setState({ type: 'dead' })

                // Se le informa a la IA correspondiente de los cambios producidos en el campo

                if ((this.props.player === '1') && (this.props.playerTwoAI))        this.props.playerTwoAI.thisUnitFieldIsDead(x, actualSection);
                else if ((this.props.player === '2') && (this.props.playerOneAI))   this.props.playerOneAI.thisUnitFieldIsDead(x, actualSection);
            }
        }
        else
        {
            let actualSection = x - section;
            const lastSection = actualSection + boat.size;

            for (actualSection; actualSection < lastSection; actualSection++)
            {
                this.state.unitsGridRefs[actualSection][y].setState({ type: 'dead' })

                // Se le informa a la IA correspondiente de los cambios producidos en el campo

                if ((this.props.player === '1') && (this.props.playerTwoAI))        this.props.playerTwoAI.thisUnitFieldIsDead(actualSection, y);
                else if ((this.props.player === '2') && (this.props.playerOneAI))   this.props.playerOneAI.thisUnitFieldIsDead(actualSection, y);
            }
        }
    }

    /* A través del uso de referencias a los elementos de la grilla y realizando el cambio de estado 
    en el elemento ya agregado evitamos renderizar nuevamente toda la grilla, que sería el caso 
    si realizamos el cambio en el estado del elemento Battlefield actualizando la unitsGrid */

    paintBoat = (x, y, paintBoat, rightButtom = false, leftButtom = false) => {
        
        if ((this.props.posibleBoat) && (!this.props.posibleBoat.onField) && (this.props.player === '1')) 
        {
            let boat            = this.props.posibleBoat;
            paintBoat.boatId    = boat.id;

            if (boat.extras === 'both')
            {
                // Si se presionaron alguno de estos dos botones en el modo de seleccion '1 Click'
                // se asigna la posición horizontal o vertical según corresponda

                if (rightButtom)        boat.horizontal = true;
                else if (leftButtom)    boat.vertical   = true;
            }

            this.drawInGrid(x, y, paintBoat, boat);
        }   
        
        // Si es la pantalla del Player 2 (CPU) y no es su turno, se muestra se muestra el puntero
        // a menos que haya hecho click, ya que en ese caso la UnitField puede haber enviado una paintBoat
        // que no deseamos poner en pantalla si no es el momento de hacerlo
        
        else if ((this.props.player !== this.props.playerTurn) && !rightButtom && !leftButtom) {
            this.paintPoint(x, y, paintBoat); 
        }
    }

    // Se llama cuando se efectúa un disparo durante el juego, la variable shootDetail recibe un objeto con los campos boatId, section y hitted

    shootMade = (x, y, paintBoat, shootDetail) => {

        let { boatId, section, hitted } = shootDetail;

        if (this.props.playerTurn !== this.props.player) // Si se efectuó un disparo en este BattleField, no fué hecho por el player dueño del mismo
        {
            this.paintPoint(x, y, paintBoat); 

            let resultOfShoot   = { message: '', playerLose: false, hitted: false, destroyed: false, playerTurn: this.props.playerTurn }; // Modelo de respuesta a enviar a los niveles de arriba
            const actualGrid    = [...this.state.unitsGrid];

            if (hitted)
            {
                const shootedBoat =  this.props.playerBoats[this.props.player].find(b => ((b.id === boatId)));

                // Chequeo de seguridad, no debería dar 'undefined' porque la variable 'hitted' está en true osea que hay un boatId

                if((shootedBoat !== undefined) && (shootedBoat.isDeadAfterHit()))
                {
                    this.killTheBoat(shootedBoat, section, x, y);

                    if (this.gameOver()) 
                    { 
                        resultOfShoot.message    = this.props.messageOnKillEnemy;
                        resultOfShoot.playerLose = true; 

                        this.props.stateOfLastShoot(resultOfShoot, actualGrid);
                        return resultOfShoot;
                    }
                    else
                    {
                        resultOfShoot.message    = this.props.messageOnDestroyShip; 
                        resultOfShoot.hitted     = true;
                        resultOfShoot.destroyed  = true;

                        this.props.stateOfLastShoot(resultOfShoot, actualGrid);
                        return resultOfShoot;
                    }
                }
                else 
                { 
                    resultOfShoot.message        = this.props.messageOnHitShip; 
                    resultOfShoot.hitted         = true;

                    this.props.stateOfLastShoot(resultOfShoot, actualGrid);
                    return resultOfShoot;
                }
            }
            else 
            { 
                resultOfShoot.message            = this.props.messageOnMissShoot; 

                this.props.stateOfLastShoot(resultOfShoot, actualGrid);
                return resultOfShoot;
            }
        }
    }

    paintPoint = (x, y, newPaint) => { this.state.unitsGridRefs[x][y].setState(newPaint) }
   
    // Recibe: posición. El bote a pintar con sus caracteristicas. Y que tipo de pintura (visible, muerto, etc) se utilizará.

    drawInGrid = (x, y, newPaint, boat) => {

        if (!boat) return false;

        // El largo del bote no debe superar la matriz en la que se lo desea dibujar
        // Se guarda en lastSection, la última unidad (parte, porción) del bote

        let lastSectionY = y + boat.size;
        let lastSectionX = x + boat.size;

        // Mostrar las dos posición solo está disponible si se está posicionando, osea, si no tiene 'type'

        if ((boat.extras === 'both') && (!newPaint.type))
        {
            if (lastSectionY <= this.state.unitsGridRefs[y].length)
            {
                let tempY       = y;
                let tempX       = x;
                let tempY2      = y;
                let tempX2      = x;
                let cleanPath   = true;
                
                for (tempY2; tempY2 < lastSectionY; tempY2++) 
                {
                    if (this.state.unitsGridRefs[tempX2][tempY2].state.hasBoat) cleanPath = false;
                }

                if (cleanPath)
                {
                    tempY = y;

                    for (tempY; tempY < lastSectionY; tempY++) 
                    {
                        this.state.unitsGridRefs[tempX][tempY].setState(newPaint);
                    }
                }
            }

            if (lastSectionX <= this.state.unitsGridRefs.length)
            {
                let tempY       = y;
                let tempX       = x;
                let tempY2      = y;
                let tempX2      = x;
                let cleanPath   = true;
                
                for (tempX2; tempX2 < lastSectionX; tempX2++) 
                {
                    if (this.state.unitsGridRefs[tempX2][tempY2].state.hasBoat) cleanPath = false;
                }

                if (cleanPath)
                {
                    for (tempX; tempX < lastSectionX; tempX++) 
                    {
                        this.state.unitsGridRefs[tempX][tempY].setState(newPaint);
                    }
                }
            }
        }
        else if ((boat.vertical) && (lastSectionY <= this.state.unitsGridRefs[y].length))
        {
            let tempY       = y;
            let tempX       = x;
            let cleanPath   = true;

            for (tempY; tempY < lastSectionY; tempY++) 
            {
                if (this.state.unitsGridRefs[tempX][tempY].state.hasBoat) cleanPath = false;
            }

            // Si se encuentra el camino libre para colocar el bote...

            if (cleanPath)
            {
                // Y si el bote es de algún tipo (osea, no solo un hover para posicionar) y
                // el bote no ha sido insertado aún..

                if ((newPaint.type) && (this.canInsertBoat(boat)))
                {
                    let section = 0;
                    for (y; y < lastSectionY; y++) 
                    {
                        newPaint.section = section;
                        this.state.unitsGridRefs[x][y].setState({...newPaint});
                        section++;
                    }
                    this.props.boatInserted(boat, this.props.player);
                    return true;
                }

                // O si el bote no es de algún tipo fijo, sino que se está mostrando una posición
                
                else if (!newPaint.type) 
                {
                    for (y; y < lastSectionY; y++) 
                    {
                        this.state.unitsGridRefs[x][y].setState(newPaint);
                    }
                }
            }
        }
        else if ((!boat.vertical) && (lastSectionX <= this.state.unitsGridRefs.length))
        {   
            let tempY       = y;
            let tempX       = x;
            let cleanPath   = true;

            for (tempX; tempX < lastSectionX; tempX++) 
            {
                if (this.state.unitsGridRefs[tempX][tempY].state.hasBoat) cleanPath = false;
            }

            // Si se encuentra el camino libre para colocar el bote...

            if (cleanPath)
            {
                // Y si el bote es de algún tipo (osea, no solo un hover para posicionar) y
                // el bote no ha sido insertado aún

                if ((newPaint.type) && (this.canInsertBoat(boat)))
                {
                    let section = 0;
                    for (x; x < lastSectionX; x++) 
                    {
                        newPaint.section = section;
                        this.state.unitsGridRefs[x][y].setState({...newPaint});
                        section++;
                    }
                    this.props.boatInserted(boat, this.props.player);
                    return true;
                }

                // O si el bote no es de algún tipo fijo, sino que se está mostrando una posición

                else if (!newPaint.type)
                {
                    for (x; x < lastSectionX; x++) 
                    {
                        this.state.unitsGridRefs[x][y].setState(newPaint);
                    }
                }
            }
        }
        return false;
    }

    canInsertBoat = boat => {

        const boatAlreadytInserted = this.props.playerBoats[this.props.player].find(b => ((b.id === boat.id)&&(!b.onField)));
        if(boatAlreadytInserted === undefined)  return false;
        else                                    return true;
    }

    gameOver = () => this.props.playerBoats[this.props.player].filter(boat => !boat.isDead()).length === 0 

    render()
    {
        const className = (this.props.player === '1') ? 'wrap-battlefield battlefield-blue' : 'wrap-battlefield battlefield-red';
        
        return (
            <div className={className}>
                {this.state.unitsGrid.map(row => row.map(unit => unit))}
            </div>
        )
    }
}

// BattleField.propTypes = 
// {
//     player:                 PropTypes.string.isRequired,
//     playerTurn:             PropTypes.string.isRequired,
//     playerOneAI:            PropTypes.instanceOf(ArtificialPlayer),
//     playerTwoAI:            PropTypes.instanceOf(ArtificialPlayer),
    
//     wide:                   PropTypes.number.isRequired,
//     longitude:              PropTypes.number.isRequired,
//     sizeAndNumberOfBoats:   PropTypes.instanceOf(Map),
    
//     posibleBoat:            PropTypes.instanceOf(Boat),
//     boatInserted:           PropTypes.func.isRequired,
//     playerBoats:            PropTypes.array,
//     stateOfLastshoot:        PropTypes.func.isRequired,

//     messageOnHitShip:       PropTypes.string,
//     messageOnDestroyShip:   PropTypes.string,
//     messageOnMissShoot:     PropTypes.string,
//     messageOnKillEnemy:     PropTypes.string
// }

export default BattleField;