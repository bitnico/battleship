import React            from 'react';

class UnitField extends React.Component
{
    /*************************************** Class Unit Field ***************************************
    
        Una Unit Field representa a cada lugar o cuadrado minimo de la grilla (o battlefield).

        Los tipos (type) a los que puede pertenecer una unidad de campo son:
            - initial (desconocida, transparente)
            - visible (para nuestro panel, podemos ver nuestros barcos)
            - water (para disparos errados)
            - hit (cuando fué golpeado)
            - dead (barco eliminado)

        La prop llamada 'type' se copia en state para ser moficado desde el padre (BattleField). 
    
    ************************************************************************************************/

    constructor(props)
    {
        super(props);

        this.state = {
            type:           this.props.type,
            positioning:    false,
            hasBoat:        false,
            boatId:         null,
            section:        null
        };
    }

    touchTheUnit = e => {

        // Show se utiliza para definir si se desea exponer la posición de la unidad al componente padre. Cosa que se 
        // evita si no se encuentra en el campo de batalla. Osea, que se está en la pantalla de configuración.
        
        if (this.props.show)
        {
            // Si la unidad se encuentra en modo inicial y pertenece al Player 1 probablemente se estén posicionando Naves

            if ((this.state.type === 'initial') && (this.props.player === '1') && (e))
            {
                // Primero debe limpiarse la posición del modo "positioning"

                this.props.paintBoat(this.props.x, this.props.y, { positioning: false }) 

                // Luego se inserta el bote

                this.props.paintBoat(this.props.x, this.props.y, { type: 'visible', hasBoat: true }, true, false)
            }
            else if (!e)
            {
                // Disparó la CPU

                if ((this.state.type === 'initial') && (!this.state.hasBoat))
                {
                    const shootDetail = { boatId : null, section: null, hitted: false };
                    
                    // Se le informa a la CPU si su disparo fué efectivo o no

                    return this.props.shootMade(this.props.x, this.props.y, { type: 'water' }, shootDetail)
                }
                else if ((this.state.type === 'visible') && (this.state.hasBoat))
                {
                    const shootDetail = { boatId : this.state.boatId, section: this.state.section, hitted: true };
                    return this.props.shootMade(this.props.x, this.props.y, { type: 'hit' }, shootDetail);
                }                
            }
            else if (this.props.player === '2')
            {
                // Disparó el usuario

                if ((this.state.type === 'initial') && (!this.state.hasBoat))
                {
                    this.props.paintBoat(this.props.x, this.props.y, { positioning: false }) 

                    const shootDetail = { boatId : null, section: null, hitted: false };
                        
                    this.props.shootMade(this.props.x, this.props.y, { type: 'water' }, shootDetail);
                }
                else if ((this.state.type === 'initial') && (this.state.hasBoat))
                {
                    this.props.paintBoat(this.props.x, this.props.y, { positioning: false }) 

                    const shootDetail = { boatId : this.state.boatId, section: this.state.section, hitted: true };
                    this.props.shootMade(this.props.x, this.props.y, { type: 'hit' }, shootDetail);
                }
            }
        }
    }

    rightTouchTheUnit = e => {     
        
        // Se utiliza solo cuando se está ingresando los botes en la modalidad "1 Click" (internamente llamado 'both')

        e.preventDefault();
        
        if ((this.props.show) && (this.state.type === 'initial') && (this.props.player === '1'))
        {
            this.props.paintBoat(this.props.x, this.props.y, { positioning: false }) 
            this.props.paintBoat(this.props.x, this.props.y, { type: 'visible', hasBoat: true }, false, true)
        }
    }

    showPosiblePosition = () => { 
        if (this.props.show)
        {
            this.props.paintBoat(this.props.x, this.props.y, { positioning: true })
        }
    }

    hidePosiblePosition = () => { 
        if (this.props.show)
        {
            this.props.paintBoat(this.props.x, this.props.y, { positioning: false }) 
        }
    }
    
    render()
    {
        let className = 'unitField ';

        if ((!this.props.show)&&(this.props.type==='initial'))  className += this.props.type;
        else if(!this.state.positioning)                        className += this.state.type;
        else                                                    className += 'positioning';

        return (
            <div 
                onContextMenu   = {this.rightTouchTheUnit}
                onClick         = {this.touchTheUnit} 
                onMouseEnter    = {this.showPosiblePosition}
                onMouseLeave    = {this.hidePosiblePosition}
                className       = {className}
            ></div>);
    }
}

export default UnitField;