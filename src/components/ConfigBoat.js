import React        from 'react';
import UnitField    from './UnitField';

class ConfigBoat extends React.Component 
{
    constructor(props)
    {
        super(props);
        this.boatUnits = this.initBoatUnits(props.boat)

        this.state = {
            boatUnits:      this.boatUnits,
            boatUnitRefs:   this.boatUnitRefs
        }
    }

    initBoatUnits(boat)
    {
        let units = [];
        for(var i=0; i<boat.size; i++) 
        {
            let type        = boat.onField ? 'initial' : 'visible';
            let unitField   = (<UnitField key={'b'+boat.id+'u'+i} show={false} type={type} player={this.props.player} />)
            units.push(unitField);
        }
        return units;
    }


    selectThisBoat = () => { 

        if (!this.props.boat.onField)
        {
            if(this.props.selectedId === this.props.boat.id) 
            {
                this.props.onSelectBoat(this.props.boat, false)
            }
            else this.props.onSelectBoat(this.props.boat, true)
        }
    }

    render()
    {
        const boatUnits = this.initBoatUnits(this.props.boat)
        const className = (this.props.selectedId === this.props.boat.id) ? 'wrap-boat selectedBoat' : 'wrap-boat';
        
        return (
            <div className={className} onClick={this.selectThisBoat.bind(this)}>
                {boatUnits.map(unitField => unitField)}
            </div>
        )
    }
}

export default ConfigBoat;