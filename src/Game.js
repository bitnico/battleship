import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';

import * as gameAC              from './redux/game/action_creators';
import * as playersAC           from './redux/players/action_creators';
import * as messagesAC          from './redux/messages/action_creators';
import * as configAC            from './redux/config/action_creators';
import Main                     from './components/Main';

const mapStateToProps = state => {

    return {
            game:               state.game,
            players:            state.players,
            field:              state.field,
            artificialPlayers:  state.artificialPlayers,
            config:             state.config,
            messages:           state.messages,
            statusBar:          (state.game.start && (state.players.actualTurn !== '')) ? 'Playing: '+state.players[state.players.actualTurn].name : state.game.status
        }
}

const mapDispachToProps = dispach => {
    return bindActionCreators({ ...gameAC, ...playersAC, ...messagesAC, ...configAC }, dispach);
}

const Game = connect(mapStateToProps, mapDispachToProps)(Main);

export default Game;